using Test
using Random

book = init_book("Tester")

name = "Name"
ingredients = ["Food", "Spices"]
instructions = "Prepare the food"
tags = ["Basic", "Unambigouus"]
@testset "Add Recipes" begin
    add_recipe!(book;
                name = name, ingredients = ingredients,
                instructions = instructions, tags = tags)

    @test_throws TypeError add_recipe!(book;
                name = name, ingredients = ingredients,
                instructions = instructions, tags = tags)

    @test_throws TypeError add_recipe!(book;
                name = "",
                ingredients = ingredients, instructions = instructions, tags = tags)

    @test_throws TypeError add_recipe!(book;
                name = "Name_2",
                ingredients = [],
                instructions = "Prepare the food",
                tags = ["Basic", "Unambigouus"])

    add_recipe!(book;
                name = "Name",
                ingredients = ["Food", "Spices"],
                instructions = "Prepare the food",
                tags = ["Basic", "Unambigouus"])

    add_recipe!(book;
                name = "Name",
                ingredients = ["Food", "Spices"],
                instructions = "Prepare the food",
                tags = ["Basic", "Unambigouus"])

    add_recipe!(book;
                name = "Name",
                ingredients = ["Food", "Spices"],
                instructions = "Prepare the food",
                tags = ["Basic", "Unambigouus"])

end
@test add_recipe!(book;
            name = "Name",
            ingredients = ["Food", "Spices"],
            instructions = "Prepare the food",
            tags = ["Basic", "Unambigouus"])
add_recipe!(book; name, ingredients, instructions)
add_recipe!(book; name, ingredients)


save(book, path)
loaded_book = load(path)

@test book == loaded_book
